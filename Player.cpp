#include "Player.h"


// Funkcja powoduje zliczanie aktualnej jednostki czasu podczas gdy uruchomimy już rozgrywkę i dostosowywujemy ją do samego gracza.
void Player::slower()
{
    static sf::Clock clock;
    static sf::Time time;

    time += clock.getElapsedTime();

    index = 0;

    if(velocity > 0 && time > sf::seconds(0.5))
    {
        time = sf::Time::Zero;
		velocity -= 2;

        if(velocity < 0)
            velocity = 0;

        clock.restart();
    }
}


// Zmiana samego życia +1 po zebraniu go na mapie.
void Player::changeLife(int value)
{
    life += value;
}

void Player::accelerate(float speed)
{
    if(velocity < acceleration*speed)
        velocity += 5;

    index = 1;
}

void Player::faster()
{
	if (acceleration < 3)
		acceleration += 0.05;
}

void Player::rotate(float angle)
{
    player[0].rotate(angle);
    player[1].rotate(angle);
}


// // Funkcja randomizuje położenie gracza w różnych miejscach mapy oraz określa jego ruch.
void Player::update(float delta)
{
	sf::Vector2f wektor;
	float radian;

    radian = (player[index].getRotation()*M_PI)/180.f;

	wektor.x = sin(radian) * delta * velocity;
    wektor.y = -cos(radian) * delta * velocity;

	player[0].move(wektor);
    player[1].move(wektor);

    rect = player[0].getGlobalBounds();
}
sf::FloatRect Player::getRect() const
{
    return rect;
}

sf::Sprite Player::getSprite() const
{
    return player[0];
}

float Player::getRotation() const
{// Funkcja randomizuje dodawanie statków obcych w różnych miejscach mapy oraz określa ich ruch bazując na głównej rozdzielczości okna.
    return player[0].getRotation();
}

void Player::addLife()
{
	if (life < 3)
		life++;
}

float Player::getVelocity() const
{
    return velocity;
}

int Player::getLife() const
{
    return life;
}

sf::Vector2f Player::getPosition() const
{
    return player[0].getPosition();
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(player[index], states);
}


// Umożliwiamy załadowanie png związanych z graczem, moedalmi statków pojawiających się na mapie.
Player::Player(const int index)
{
    velocity = 0;
	acceleration = 2.8;
    life = 3;

    track[0] = "data/textures/playerShip1.png";
    track[1] = "data/textures/playerShip2.png";
    track[2] = "data/textures/playerShip3.png";


    track[12] = "data/textures/playerShip1.png";
    track[13] = "data/textures/playerShip2.png";
    track[14] = "data/textures/playerShip3.png";



    texture[0].loadFromFile(track[index]);
    texture[1].loadFromFile(track[index+12]);

    for(int i=0; i<2; i++)
    {
        texture[i].setSmooth(true);
        player[i].setTexture(texture[i]);
        player[i].setPosition((1280-player[0].getGlobalBounds().width)/2, 720/2);
    }

    rect = player[0].getGlobalBounds();
    player[0].setOrigin(rect.width/2, rect.height/2);
    player[1].setOrigin(rect.width/2, rect.height/2);

}

